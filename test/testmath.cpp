#include "gtest/gtest.h"

#include "math/vector3.hpp"

namespace art {
    TEST(TestMath, test_vector_projection) {
        const Vec3d vec_a = Vec3d(0.5, 0.5, 0.5);
        const Vec3d vec_b = normalized(Vec3d(0.5, 0.5, 0.0));

        EXPECT_EQ(project(vec_a, vec_b), Vec3d(0.5, 0.5, 0.0));
    }

    TEST(TestMath, test_vector_normalization) {
        const Vec3d vec = Vec3d(233.042, 12445.23, 521.0004);
        const Vec3d vec_normalized = normalized(vec);

        EXPECT_LE(abs(magnitude(vec_normalized) - 1.0), 1.0e-14);
    }
}