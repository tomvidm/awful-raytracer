#include "gtest/gtest.h"

#include <iomanip>
#include <iostream>
#include <memory>
#include <cmath>

#include "shapes/infinitecylinder.hpp"
#include "material.hpp"
#include "constants.hpp"
#include "interaction.hpp"
#include "math/linearfunction.hpp"

namespace art {
    TEST(TestShapes, plane_infinitecylinder) {
        const InfiniteCylinder cyl(Vec3d(), Vec3d(0.0, 0.0, 1.0), 2.0);

        const Vec3d a(5.0, 0.0, 5.0);

        const SurfacePoint a_proj_cyl = cyl.getNearestSurfacePoint(a);

        EXPECT_TRUE(almostEqual(a_proj_cyl.normal, Vec3d(1.0, 0.0, 0.0)));
        EXPECT_TRUE(almostEqual(a_proj_cyl.point, Vec3d(2.0, 0.0, 5.0)));

        const Ray ray_a = Ray(a, Vec3d(-1.0, 0.0, 0.0), 540.0);
        const Ray ray_b = Ray(a, Vec3d(1.0, 0.0, 0.0), 540.0);
        const Ray ray_c = Ray(Vec3d(0.0, 0.0, 5.0), Vec3d(1.0, 0.0, 0.0), 540.0);

        const RayShapeIntersection isec_a = cyl.getRayIntersection(ray_a);
        const RayShapeIntersection isec_b = cyl.getRayIntersection(ray_b);
        const RayShapeIntersection isec_c = cyl.getRayIntersection(ray_c);

        EXPECT_TRUE(isec_a.rayHit);
        EXPECT_TRUE(almostEqual(isec_a.point, Vec3d(2.0, 0.0, 5.0)));
        EXPECT_TRUE(almostEqual(isec_a.normal, Vec3d(1.0, 0.0, 0.0)));

        EXPECT_FALSE(isec_b.rayHit);

        EXPECT_TRUE(isec_c.rayHit);
        EXPECT_TRUE(almostEqual(isec_c.point, Vec3d(2.0, 0.0, 5.0)));
        std::cout << isec_c.normal << std::endl;
        EXPECT_TRUE(almostEqual(isec_c.normal, Vec3d(-1.0, 0.0, 0.0)));
    }
}