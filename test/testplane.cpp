#include "gtest/gtest.h"

#include <iomanip>
#include <iostream>
#include <memory>
#include <cmath>

#include "shapes/plane.hpp"
#include "material.hpp"
#include "constants.hpp"
#include "interaction.hpp"
#include "math/linearfunction.hpp"

namespace art {
    TEST(TestShapes, plane_projection) {
        const Vec3d zeroVector = Vec3d(0.0, 0.0, 0.0);

        const Vec3d xunit = Vec3d(1.0, 0.0, 0.0);
        const Vec3d yunit = Vec3d(0.0, 1.0, 0.0);
        const Vec3d zunit = Vec3d(0.0, 0.0, 1.0);

        const Plane xy = Plane(zeroVector, zunit);
        const Plane xy_neg = Plane(zeroVector, -zunit);

        const Vec3d somewhere = Vec3d(15.0, 15.0, 1255.0323);

        const SurfacePoint somewhereProjected = xy.getNearestSurfacePoint(somewhere);
        const SurfacePoint somewhereProjectedNeg = xy_neg.getNearestSurfacePoint(somewhere);

        EXPECT_TRUE(almostEqual(somewhereProjected.point, Vec3d(15.0, 15.0, 0.0)));
        EXPECT_TRUE(almostEqual(somewhereProjected.normal, zunit));

        EXPECT_TRUE(almostEqual(somewhereProjectedNeg.point, Vec3d(15.0, 15.0, 0.0)));
        EXPECT_TRUE(almostEqual(somewhereProjectedNeg.normal, zunit));
    }
}