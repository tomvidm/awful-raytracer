#include "gtest/gtest.h"

#include <iomanip>
#include <iostream>
#include <memory>
#include <cmath>

#include "shapes/plane.hpp"
#include "material.hpp"
#include "constants.hpp"
#include "interaction.hpp"
#include "math/linearfunction.hpp"

namespace art {
    TEST(TestInteraction, TIR_check) {
        const double n1 = 1.3325;
        const double n2 = 1.0;
        const double criticalAngle = asin(n2 / n1);
        const double radToDeg = 180.0 / PI;
        const double goodAngle = PI + criticalAngle - 0.1;
        const double badAngle = PI + criticalAngle + 0.1;

        const Plane xy = Plane(Vec3d(0.0, 0.0, 0.0), Vec3d(0.0, 0.0, 1.0));
        
        const Ray rayTIR = Ray(Vec3d(0.0, 0.0, 1.0), Vec3d::fromSpherical(1.0, badAngle, 0.0), 540);
        const Ray rayRT = Ray(Vec3d(0.0, 0.0, 1.0), Vec3d::fromSpherical(1.0, goodAngle, 0.0), 540);

        std::shared_ptr<ComplexFunction1D> ior1 = std::make_shared<ComplexLinearFunction1D>(0.0, 1.3325);
        std::shared_ptr<ComplexFunction1D> ior2 = std::make_shared<ComplexLinearFunction1D>(0.0, 1.0);

        std::shared_ptr<Material> mat1 = std::make_shared<Material>(ior1);
        std::shared_ptr<Material> mat2 = std::make_shared<Material>(ior2);

        RayShapeIntersection intersectionTIR = xy.getRayIntersection(rayTIR);
        Interaction interactionTIR = Interaction(intersectionTIR, mat1, mat2);
        EXPECT_EQ(interactionTIR.getInteractionType(), InteractionType::TotalInternalReflection);

        RayShapeIntersection intersectionRT = xy.getRayIntersection(rayRT);
        Interaction interactionRT = Interaction(intersectionRT, mat1, mat2);
        EXPECT_EQ(interactionRT.getInteractionType(), InteractionType::ReflectAndTransmit);
    }
}