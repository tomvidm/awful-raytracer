cmake_minimum_required(VERSION 3.9)

project(art)

set(GTEST_DIR ${CMAKE_SOURCE_DIR}/external/googletest/googletest)

add_subdirectory(external/googletest)

set(CMAKE_CXX_STANDARD 11)

if (WIN32)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
endif(WIN32)


set(SOURCES
    src/emitter.cpp
    src/material.cpp
    src/volume.cpp
    #src/scene.cpp
    src/interaction.cpp
    src/sensor.cpp
    src/shapes/plane.cpp
    src/shapes/sphere.cpp
    src/shapes/infiniteslab.cpp
    src/shapes/infinitecylinder.cpp
    src/math/aabb.cpp
    src/math/vector3.cpp
    src/math/matrix3.cpp
    src/math/matrix4.cpp
    src/math/transform3.cpp
    src/math/transform4.cpp
)

set(TEST_SOURCES
    test/testmath.cpp
    test/testplane.cpp
    test/testinteraction.cpp
    test/testinfinitecylinder.cpp
)

include_directories(
    src
    external/CImg
)

add_library(
    art
    ${SOURCES}
)

add_executable(
    art-exec
    src/main.cpp
)

target_link_libraries(
    art-exec
    art
)

add_executable(
    art-test
    ${TEST_SOURCES}
)

target_link_libraries(
    art-test
    art
    gtest_main
    gtest
)


