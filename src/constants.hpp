#pragma once

#include <cmath>

namespace art {
    const double epsilon = 0.0000005;

    const double PI = 3.14159265359;
    const double halfPI = 1.57079632679;
    const double doublePI = 6.28318530718;

    const double freeSpacePermittivity = 8.85418781762039e-12;
    const double freeSpacePermeability = 1.2566370614e-6;
    const double freeSpaceImpedance = std::sqrt(freeSpacePermeability / freeSpacePermittivity);
    const double speedOfLight = 1.0 / std::sqrt(freeSpacePermeability * freeSpacePermittivity);
}