#include "sensor.hpp"
#include <iostream>
#include <cmath>

using namespace cimg_library;

namespace art {
    LuminanceMap::LuminanceMap(const unsigned ures, const unsigned vres) {
        luminanceBitmap = CImg<double>(ures, vres, 1, 3);
    }

    void LuminanceMap::add(const double u, const double v, const unsigned c, const double l) {
        luminanceBitmap.set_linear_atXY(l, static_cast<float>(u), static_cast<float>(v), 0, c, true);
    }
    
    void LuminanceMap::save(const char  * const filename) {
        const double stdev = sqrt(luminanceBitmap.variance());
        std::cout << "(min, max) == (" << luminanceBitmap.min() << ", " << luminanceBitmap.max() << ")" << std::endl;
        std::cout << "mean value == " << luminanceBitmap.mean() << std::endl;
        std::cout << "std        == " << stdev << std::endl;
        //luminanceBitmap.normalize(0.0, 255.0);
        luminanceBitmap.normalize(0.0, 255.0);
        luminanceBitmap.save(filename);
    }
}