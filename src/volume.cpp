#include "volume.hpp"

namespace art {
    RayVolumeInteraction Volume::getRayInteraction(const Ray& ray, std::shared_ptr<Material>& currentRayMaterial) const {
        const RayShapeIntersection isec = boundingShape->getRayIntersection(ray);
        return getRayInteraction(isec, currentRayMaterial);
    }

    RayVolumeInteraction Volume::getRayInteraction(const RayShapeIntersection& isec, std::shared_ptr<Material>& currentRayMaterial) const {
        if (isec.rayHit) {
            Interaction interaction(isec, currentRayMaterial, material);
            return RayVolumeInteraction{
                true,
                Interaction(isec, currentRayMaterial, material),
                isec,
                currentRayMaterial,
                material    
            };
        } else {
            return RayVolumeInteraction{
                false
            };
        }
    }
}