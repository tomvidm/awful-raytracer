#pragma once

#include <memory>
#include <vector>

#include "CImg.h"
#include "shapes/shape.hpp"
#include "math/functioninterface.hpp"

namespace art {

    class LuminanceMap {
    public:
        LuminanceMap() = delete;
        LuminanceMap(const unsigned ures, const unsigned vres);

        void add(const double u, const double v, const unsigned c, const double l);
        void save(const char* const filename);
    private:
        cimg_library::CImg<double> luminanceBitmap;
    };
}