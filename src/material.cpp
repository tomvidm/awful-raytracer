#include "material.hpp"

namespace art {
    std::complex<double> Material::getIndexOfRefraction(const double wavelength) const {
        return iorByWavelength->eval(wavelength);
    }
}