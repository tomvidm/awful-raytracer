#pragma once

#include "math/vector3.hpp"
#include "math/matrix3.hpp"

namespace art {
    using RGBColor = Vec3d;
    using XYZColor = Vec3d;

    const Mat3 XYZtoRGB_matrix(
        3.240479, -1.53715, -0.498535,
        -0.969256, 1.875991, 0.041556,
        0.055648, -0.204043, 1.057311
    );

    RGBColor XYZtoRGB(const XYZColor& xyz) {
        return XYZtoRGB_matrix.transform(xyz);
    }
}