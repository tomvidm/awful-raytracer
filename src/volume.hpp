#pragma once

#include <memory>
#include <vector>

#include "material.hpp"
#include "shapes/shape.hpp"
#include "interaction.hpp"
#include "math/vector3.hpp"

namespace art {
    struct RayVolumeInteraction {
        bool rayHit;
        Interaction interaction;
        RayShapeIntersection isec;
        std::shared_ptr<Material> exitingMaterial;
        std::shared_ptr<Material> enteringMaterial;
    };


    class Volume {
    public:
        Volume() = delete;
        Volume(std::shared_ptr<Material>& mat, std::shared_ptr<Shape>& shape)
        : material(mat), boundingShape(shape) {}

        RayVolumeInteraction getRayInteraction(const Ray& ray, std::shared_ptr<Material>& currentRayMaterial) const;
        RayVolumeInteraction getRayInteraction(const RayShapeIntersection& isec, std::shared_ptr<Material>& currentRayMaterial) const;
    private:
        mutable std::shared_ptr<Material> material;
        mutable std::shared_ptr<Shape> boundingShape;
    };
}