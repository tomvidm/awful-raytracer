#include "aabb.hpp"

#include <algorithm>

namespace art {
    bool AABB::contains(const Vec3d& point) const {
        const double x0 = std::min(origin.x, origin.x + size.x);
        const double x1 = std::max(origin.x, origin.x + size.x);
        const double y0 = std::min(origin.y, origin.y + size.y);
        const double y1 = std::max(origin.y, origin.y + size.y);
        const double z0 = std::min(origin.z, origin.z + size.z);
        const double z1 = std::max(origin.z, origin.z + size.z);

        if (point.x < x0 || point.x > x1
         || point.y < y0 || point.y > y1
         || point.z < z0 || point.z > z1) {
            return false;
        } else {
            return true;
        }
    }
}