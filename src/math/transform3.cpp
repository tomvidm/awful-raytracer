#include "transform3.hpp"

namespace art {
    Transform3::Transform3() 
    : mat(), invMat() {
        ;
    }

    Transform3::Transform3(const Mat3& matrix) 
    : mat(matrix) {
        invMat = mat.inverse();
        invTranspose = invMat.transpose();
    }
}