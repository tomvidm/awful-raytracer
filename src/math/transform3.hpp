#pragma once

#include "matrix3.hpp"
#include "vector3.hpp"

namespace art {
    class Transform3 {
    public:
        Transform3();
        Transform3(const Mat3& matrix);

        inline const Mat3& getTransform() const { return mat; }
        inline const Mat3& getInverseTransform() const { return invMat; }
        inline const Mat3& getInverseTranspose() const { return invTranspose; }
    private:
        Mat3 mat;
        Mat3 invMat;
        Mat3 invTranspose;
    };
}