#include "matrix4.hpp"

namespace art {
    Mat4::Mat4()
    : m00(1.0), m01(0.0), m02(0.0), m03(0.0),
      m10(0.0), m11(1.0), m12(0.0), m13(0.0),
      m20(0.0), m21(0.0), m22(1.0), m23(0.0),
      m30(0.0), m31(0.0), m32(0.0), m33(1.0) {}

    Mat4::Mat4(double v00, double v01, double v02, double v03,
               double v10, double v11, double v12, double v13,
               double v20, double v21, double v22, double v23,
               double v30, double v31, double v32, double v33)
    : m00(v00), m01(v01), m02(v02), m03(v03),
      m10(v10), m11(v11), m12(v12), m13(v13),
      m20(v20), m21(v21), m22(v22), m23(v23),
      m30(v30), m31(v31), m32(v32), m33(v33) {}

    Mat4::Mat4(const  Vec3d& u, const  Vec3d& v, const  Vec3d& w)
    : m00(u.x), m01(v.x), m02(w.x), m03(0.0),
      m10(u.y), m11(v.y), m12(w.y), m13(0.0),
      m20(u.z), m21(v.z), m22(w.z), m23(0.0),
      m30(0.0), m31(0.0), m32(0.0), m33(1.0) {}

    Mat4::Mat4(const  Vec4d& u, const  Vec4d& v, const  Vec4d& w, const  Vec4d& ww)
    : m00(u.x), m01(v.x), m02(w.x), m03(ww.x),
      m10(u.y), m11(v.y), m12(w.y), m13(ww.y),
      m20(u.z), m21(v.z), m22(w.z), m23(ww.z),
      m30(u.w), m31(v.w), m32(w.w), m33(ww.w) {}

     Vec3d Mat4::transform(const Vec3d& vec) const {
        return Vec3d(
            m00 * vec.x + m01 * vec.y + m02 * vec.z + m03,
            m10 * vec.x + m11 * vec.y + m12 * vec.z + m13,
            m20 * vec.x + m21 * vec.y + m22 * vec.z + m23
        );
    }

     Vec4d Mat4::transform(const Vec4d& vec) const {
        return Vec4d(
            m00 * vec.x + m01 * vec.y + m02 * vec.z + m03 * vec.w,
            m10 * vec.x + m11 * vec.y + m12 * vec.z + m13 * vec.w,
            m20 * vec.x + m21 * vec.y + m22 * vec.z + m23 * vec.w,
            m30 * vec.x + m31 * vec.y + m32 * vec.z + m33 * vec.w
        );
    }

    double Mat4::determinant() const {
        return m03 * m12 * m21 * m30 - m02 * m13 * m21 * m30 -
               m03 * m11 * m22 * m30 + m01 * m13 * m22 * m30 +
               m02 * m11 * m23 * m30 - m01 * m12 * m23 * m30 -
               m03 * m12 * m20 * m31 + m02 * m13 * m20 * m31 +
               m03 * m10 * m22 * m31 - m00 * m13 * m22 * m31 -
               m02 * m10 * m23 * m31 + m00 * m12 * m23 * m31 +
               m03 * m11 * m20 * m32 - m01 * m13 * m20 * m32 -
               m03 * m10 * m21 * m32 + m00 * m13 * m21 * m32 +
               m01 * m10 * m23 * m32 - m00 * m11 * m23 * m32 -
               m02 * m11 * m20 * m33 + m01 * m12 * m20 * m33 +
               m02 * m10 * m21 * m33 - m00 * m12 * m21 * m33 -
               m01 * m10 * m22 * m33 + m00 * m11 * m22 * m33;
    }

    Mat4 Mat4::inverse() const {
        double scale = 1.0 / determinant();

        double n00 = scale * (m12 * m23 * m31 - m13 * m22 * m31 + m13 * m21 * m32 - m11 * m23 * m32 - m12 * m21 * m33 + m11 * m22 * m33);
        double n01 = scale * (m03 * m22 * m31 - m02 * m23 * m31 - m03 * m21 * m32 + m01 * m23 * m32 + m02 * m21 * m33 - m01 * m22 * m33);
        double n02 = scale * (m02 * m13 * m31 - m03 * m12 * m31 + m03 * m11 * m32 - m01 * m13 * m32 - m02 * m11 * m33 + m01 * m12 * m33);
        double n03 = scale * (m03 * m12 * m21 - m02 * m13 * m21 - m03 * m11 * m22 + m01 * m13 * m22 + m02 * m11 * m23 - m01 * m12 * m23);
        double n10 = scale * (m13 * m22 * m30 - m12 * m23 * m30 - m13 * m20 * m32 + m10 * m23 * m32 + m12 * m20 * m33 - m10 * m22 * m33);
        double n11 = scale * (m02 * m23 * m30 - m03 * m22 * m30 + m03 * m20 * m32 - m00 * m23 * m32 - m02 * m20 * m33 + m00 * m22 * m33);
        double n12 = scale * (m03 * m12 * m30 - m02 * m13 * m30 - m03 * m10 * m32 + m00 * m13 * m32 + m02 * m10 * m33 - m00 * m12 * m33);
        double n13 = scale * (m02 * m13 * m20 - m03 * m12 * m20 + m03 * m10 * m22 - m00 * m13 * m22 - m02 * m10 * m23 + m00 * m12 * m23);
        double n20 = scale * (m11 * m23 * m30 - m13 * m21 * m30 + m13 * m20 * m31 - m10 * m23 * m31 - m11 * m20 * m33 + m10 * m21 * m33);
        double n21 = scale * (m03 * m21 * m30 - m01 * m23 * m30 - m03 * m20 * m31 + m00 * m23 * m31 + m01 * m20 * m33 - m00 * m21 * m33);
        double n22 = scale * (m01 * m13 * m30 - m03 * m11 * m30 + m03 * m10 * m31 - m00 * m13 * m31 - m01 * m10 * m33 + m00 * m11 * m33);
        double n23 = scale * (m03 * m11 * m20 - m01 * m13 * m20 - m03 * m10 * m21 + m00 * m13 * m21 + m01 * m10 * m23 - m00 * m11 * m23);
        double n30 = scale * (m12 * m21 * m30 - m11 * m22 * m30 - m12 * m20 * m31 + m10 * m22 * m31 + m11 * m20 * m32 - m10 * m21 * m32);
        double n31 = scale * (m01 * m22 * m30 - m02 * m21 * m30 + m02 * m20 * m31 - m00 * m22 * m31 - m01 * m20 * m32 + m00 * m21 * m32);
        double n32 = scale * (m02 * m11 * m30 - m01 * m12 * m30 - m02 * m10 * m31 + m00 * m12 * m31 + m01 * m10 * m32 - m00 * m11 * m32);
        double n33 = scale * (m01 * m12 * m20 - m02 * m11 * m20 + m02 * m10 * m21 - m00 * m12 * m21 - m01 * m10 * m22 + m00 * m11 * m22);

        return Mat4(n00, n01, n02, n03,
                    n10, n11, n12, n13,
                    n20, n21, n22, n23,
                    n30, n31, n32, n33);
    }

    
    Mat4 Mat4::transpose() const {
        return Mat4(
            m00, m10, m20, m30,
            m01, m11, m21, m31,
            m02, m12, m22, m32,
            m03, m13, m23, m33
        );
    }

    Mat4 Mat4::compose(const Mat4& rhs) const {
        const Vec4d col0 = transform(Vec4d(rhs.m00, rhs.m10, rhs.m20, rhs.m30));
        const Vec4d col1 = transform(Vec4d(rhs.m01, rhs.m11, rhs.m21, rhs.m31));
        const Vec4d col2 = transform(Vec4d(rhs.m02, rhs.m12, rhs.m22, rhs.m32));
        const Vec4d col3 = transform(Vec4d(rhs.m03, rhs.m13, rhs.m23, rhs.m33));

        return Mat4(col0, col1, col2, col3);
    }
}