#pragma once

#include "vector3.hpp"

namespace art {
    class AABB {
    public:
        bool contains(const Vec3d& point) const;
    private:
        const Vec3d origin;
        const Vec3d size;
    };
}