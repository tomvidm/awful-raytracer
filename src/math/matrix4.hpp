#pragma once

#include "vector3.hpp"
#include "vector4.hpp"

namespace art {
    class Mat4
    {
    public:
        double m00, m01, m02, m03;
        double m10, m11, m12, m13;
        double m20, m21, m22, m23;
        double m30, m31, m32, m33;
    public:
        Mat4();
        Mat4(double v00, double v01, double v02, double v03,
             double v10, double v11, double v12, double v13,
             double v20, double v21, double v22, double v23,
             double v30, double v31, double v32, double v33);
        Mat4(const Vec3<double>& u, const Vec3<double>& v, const Vec3<double>& w);
        Mat4(const Vec4<double>& u, const Vec4<double>& v, const Vec4<double>& w, const Vec4<double>& ww);

        Vec3<double> transform(const Vec3<double>& vec) const;
        Vec4<double> transform(const Vec4<double>& vec) const;
        double determinant() const;
        Mat4 inverse() const;
        Mat4 transpose() const;
        Mat4 compose(const Mat4& rhs) const;
    };
}