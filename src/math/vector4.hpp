#pragma once

namespace art {
    template <typename T>
    class Vec4 {
    public:
        Vec4() : x(T(0)), y(T(0)), z(T(0)), w(T(0)) {}
        Vec4(const T x, const T y, const T z, const T w) : x(x), y(y), z(z), w(w) {}
        Vec4(const Vec4<T>& other) : x(other.x), y(other.y), z(other.z), w(other.w) {}
    public:
        T x;
        T y;
        T z;
        T w;
    };

    using Vec4i = Vec4<int>;
    using Vec4u = Vec4<unsigned>;
    using Vec4f = Vec4<float>;
    using Vec4d = Vec4<double>;
}