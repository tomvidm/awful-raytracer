#pragma once

#include "matrix4.hpp"
#include "vector3.hpp"

namespace art {
    class Transform4 {
    public:
        Transform4();
        Transform4(const Mat4& matrix);

        inline const Mat4& getTransform() const { return mat; }
        inline const Mat4& getInverseTransform() const { return invMat; }
        inline const Mat4& getInverseTranspose() const { return invTransposeMat; }
    private:
        Mat4 mat;
        Mat4 invMat;
        Mat4 invTransposeMat;
    };
}