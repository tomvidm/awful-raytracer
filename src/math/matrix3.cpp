#include "matrix3.hpp"

namespace art {
    Mat3::Mat3()
    : m00(1.0), m01(0.0), m02(0.0),
      m10(0.0), m11(1.0), m12(0.0),
      m20(0.0), m21(0.0), m22(1.0) {}

    Mat3::Mat3(double v00, double v01, double v02,
               double v10, double v11, double v12,
               double v20, double v21, double v22)
    : m00(v00), m01(v01), m02(v02),
      m10(v10), m11(v11), m12(v12),
      m20(v20), m21(v21), m22(v22) {}

    Mat3::Mat3(Vec3<double> u, Vec3<double> v, Vec3<double> w)
    : m00(u.x), m01(v.x), m02(w.x),
      m10(u.y), m11(v.y), m12(w.y),
      m20(u.z), m21(v.z), m22(w.z) {}

    Vec3<double> Mat3::transform(const Vec3<double>& vec) const {
        return Vec3<double>(
            m00 * vec.x + m01 * vec.y + m02 * vec.z,
            m10 * vec.x + m11 * vec.y + m12 * vec.z,
            m20 * vec.x + m21 * vec.y + m22 * vec.z
        );
    }

    double Mat3::determinant() const {
        return m00 * (m11 * m22 - m21 * m12)
             - m01 * (m10 * m22 - m12 * m20)
             + m02 * (m10 * m21 - m11 * m20);
    }

    Mat3 Mat3::inverse() const {
        double scale = 1.0 / determinant();

        double v00 = scale * (m11 *  m22 -  m21 * m12);
        double v01 = scale * (m02 *  m21 -  m01 * m22);
        double v02 = scale * (m01 *  m12 -  m02 * m11);
        double v10 = scale * (m12 *  m20 -  m10 * m22);
        double v11 = scale * (m00 *  m22 -  m02 * m20);
        double v12 = scale * (m10 *  m02 -  m00 * m12);
        double v20 = scale * (m10 *  m21 -  m20 * m11);
        double v21 = scale * (m20 *  m01 -  m00 * m21);
        double v22 = scale * (m00 *  m11 -  m10 * m01);

        return Mat3(v00, v01, v02,
                    v10, v11, v12,
                    v20, v21, v22);
    }

    Mat3 Mat3::transpose() const {
        return Mat3(
            m00, m10, m20,
            m01, m11, m21,
            m02, m12, m22
        );
    }

    Mat3 Mat3::compose(const Mat3& rhs) const {
        const Vec3d col0 = transform(Vec3d(rhs.m00, rhs.m10, rhs.m20));
        const Vec3d col1 = transform(Vec3d(rhs.m01, rhs.m11, rhs.m21));
        const Vec3d col2 = transform(Vec3d(rhs.m02, rhs.m12, rhs.m22));

        return Mat3(col0, col1, col2);
    }
}