#include "vector3.hpp"

namespace art {
    // phi = 0 -> +x
    // theta = 0 -> +z
    Vec3<double> Vec3<double>::fromSpherical(const double length, const double theta, const double phi) {
        return Vec3d(
            length * sin(theta) * cos(phi),
            length * sin(theta) * sin(phi),
            length * cos(theta)
        );
    }

    Vec3d project(const Vec3d& a, const Vec3d& b) {
        const double invLengthSquared = 1.0 / magnitudeSquared(b);
        return invLengthSquared * dot(a, b) * b;
    }

    double magnitude(const Vec3d& vec) {
        return std::sqrt(magnitudeSquared(vec));
    }

    Vec3d normalized(const Vec3d& vec) {
        double invLength = 1.0 / magnitude(vec);
        return Vec3<double>(
            vec.x * invLength,
            vec.y * invLength,
            vec.z * invLength
        );
    }

    Vec3d reflectAboutNormal(const Vec3d& vec, const Vec3d& norm) {
        return 2.0 * dot(vec, norm) * norm - vec;
    }

    Vec3d reflectAboutVector(const Vec3d& vec, const Vec3d& refl) {
        const Vec3d norm = normalized(refl);
        return 2.0 * dot(vec, norm) * norm - vec;
    }

    bool almostEqual(const Vec3d a, const Vec3d b, const double epsilon) {
        if ((abs(a.x - b.x) < epsilon)
         && (abs(a.y - a.y) < epsilon)
         && (abs(a.z - b.z) < epsilon)) {
             return true;
        } else {
            return false;
        }
    }
}