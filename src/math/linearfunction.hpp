#pragma once

#include "functioninterface.hpp"

namespace art {
    template <typename T>
    class LinearFunction1D : public Function1D<T> {
    public:
        LinearFunction1D() = delete;
        LinearFunction1D(const T a, const T b) : a(a), b(b) {}
        virtual T eval(const double arg) const;
    private:
        const T a;
        const T b;
    };

    using ComplexLinearFunction1D = LinearFunction1D<std::complex<double>>;

    template <typename T>
    T LinearFunction1D<T>::eval(const double arg) const {
        return a * static_cast<T>(arg) + b;
    }
}