#include "transform4.hpp"

namespace art {
    Transform4::Transform4() 
    : mat(), invMat() {
        ;
    }

    Transform4::Transform4(const Mat4& matrix) 
    : mat(matrix) {
        invMat = mat.inverse();
        invTransposeMat = invMat.transpose();
    }
}