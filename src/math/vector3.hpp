#pragma once

#include <cmath>
#include <string>

namespace art {
    template <typename T>
    class Vec3 {
    public:
        Vec3() : x(T(0)), y(T(0)), z(T(0)) {;}
        Vec3(const T x, const T y, const T z) : x(x), y(y), z(z) {;}
        Vec3(const Vec3<T>& other) : x(other.x), y(other.y), z(other.z) {;}

        static Vec3<double> fromSpherical(const double length, const double theta, const double phi);

        void operator = (const Vec3<T>& other);

        template <typename T>
        friend std::ostream& operator << (std::ostream& stream, const Vec3<T>& vec);
    public:
        T x;
        T y;
        T z;
    };

    using Vec3i = Vec3<int>;
    using Vec3u = Vec3<unsigned>;
    using Vec3f = Vec3<float>;
    using Vec3d = Vec3<double>;

    template <typename T>
    void Vec3<T>::operator = (const Vec3<T>& other) {
        x = other.x;
        y = other.y;
        z = other.z;
    }

    template <typename T>
    std::ostream& operator << (std::ostream& stream, const Vec3<T>& vec) {
        stream << vec.x << ", " << vec.y << ", " << vec.z;
        return stream;
    }

    template <typename T>
    bool operator == (const Vec3<T>& lhs, const Vec3<T>& rhs) {
        if ((lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z)) {
            return true;
        } else {
            return false;
        }
    }

    template <typename T>
    T dot(const Vec3<T>& lhs, const Vec3<T>& rhs) {
        return lhs * rhs;
    }

    template <typename T>
    Vec3<T> cross(const Vec3<T>& lhs, const Vec3<T>& rhs) {
        return Vec3<T>(
            lhs.y * rhs.z - lhs.z * rhs.y,
            lhs.z * rhs.x - lhs.x * rhs.z,
            lhs.x * rhs.y - lhs.y * rhs.x
        );
    }

    template <typename T>
    Vec3<T> operator * (const T lhs, const Vec3<T>& rhs) {
        return Vec3<T>(
            lhs * rhs.x,
            lhs * rhs.y,
            lhs * rhs.z
        );
    }

    template <typename T>
    Vec3<T> operator + (const Vec3<T>& lhs, const Vec3<T>& rhs) {
        return Vec3<T>(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
    }

    template <typename T>
    Vec3<T> operator - (const Vec3<T>& lhs, const Vec3<T>& rhs) {
        return Vec3<T>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
    }

    template <typename T>
    T magnitudeSquared(const Vec3<T>& vec) {
        return vec.x * vec.x + vec.y * vec.y + vec.z * vec.z;
    }

    template <typename T>
    T operator * (const Vec3<T>& lhs, const Vec3<T>& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
    }

    template <typename T>
    Vec3<T> operator - (const Vec3<T>& vec) {
        return Vec3<T>(-vec.x, -vec.y, -vec.z);
    }

    template <typename T>
    Vec3<T> projectToX(const Vec3<T>& vec) {
        return Vec3<T>(vec.x, 0.0, 0.0);
    }

    template <typename T>
    Vec3<T> projectToY(const Vec3<T>& vec) {
        return Vec3<T>(0.0, vec.y, 0.0);
    }

    template <typename T>
    Vec3<T> projectToZ(const Vec3<T>& vec) {
        return Vec3<T>(0.0, 0.0, vec.z);
    }

    Vec3d project(const Vec3d& a, const Vec3d& b);

    double magnitude(const Vec3d& vec);

    Vec3d normalized(const Vec3d& vec);

    Vec3d reflectAboutNormal(const Vec3d& vec, const Vec3d& norm);

    Vec3d reflectAboutVector(const Vec3d& vec, const Vec3d& refl);

    bool almostEqual(const Vec3d a, const Vec3d b, const double epsilon = 0.0000000001);
}