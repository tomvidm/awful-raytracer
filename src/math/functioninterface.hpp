#pragma once

#include <memory>
#include <complex>

namespace art {
    template <typename T>
    class FunctionInterface {
    public:
        virtual T eval(const double arg) const = 0;
    };

    template <typename T>
    using Function1D = FunctionInterface<T>;
    
    using RealFunction1D = FunctionInterface<double>;
    using ComplexFunction1D = FunctionInterface<std::complex<double>>;
}