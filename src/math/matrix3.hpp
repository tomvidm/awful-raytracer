#pragma once

#include "vector3.hpp"

namespace art {
    class Mat3
    {
    public:
        double m00, m01, m02;
        double m10, m11, m12;
        double m20, m21, m22;
    public:
        Mat3();
        Mat3(double v00, double v01, double v02,
             double v10, double v11, double v12,
             double v20, double v21, double v22);
        Mat3(Vec3<double> u, Vec3<double> v, Vec3<double> w);

        Vec3<double> transform(const Vec3<double>& vec) const;
        
        double determinant() const;
        Mat3 inverse() const;
        Mat3 transpose() const;
        Mat3 compose(const Mat3& rhs) const;
    };
}