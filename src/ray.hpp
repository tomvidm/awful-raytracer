#pragma once

#include "math/vector3.hpp"

namespace art {
    class Ray {
    public:
        Ray() : origin(), direction(), wavelength(0.0), relativeIntensity(0.0) {}

        Ray(
            const Vec3d& origin, 
            const Vec3d& direction, 
            const double wl) 
        : origin(origin), 
          direction(direction), 
          wavelength(wl), 
          relativeIntensity(1.0) {}

        Ray(
            const Vec3d& origin, 
            const Vec3d& direction, 
            const double wl, 
            const double intensity) 
        : origin(origin), 
          direction(direction), 
          wavelength(wl), 
          relativeIntensity(intensity) {}
        
        Ray(const Ray& other) : origin(other.origin), direction(other.direction), wavelength(other.wavelength) {}

        Vec3d origin;
        Vec3d direction;
        double wavelength;
        double relativeIntensity;
    };
}