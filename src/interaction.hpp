#pragma once

#include <memory>
#include <random>

#include "shapes/shape.hpp"
#include "material.hpp"

namespace art {
    enum InteractionType {
        ReflectAndTransmit,
        TotalInternalReflection
    };

    struct RaySample {
        bool transmitted;
        Ray ray;
    };

    class Interaction {
    public:
        Interaction() {}
        Interaction(const RayShapeIntersection& intersection, 
                    std::shared_ptr<Material>& exteriorMaterial, 
                    std::shared_ptr<Material>& interiorMaterial);

        inline Ray getReflectedRay() const { return reflectedRay; }
        inline Ray getTransmittedRay() const { return transmittedRay; }

        inline double getReflectance() const { return reflectance; }
        inline double getTransmittance() const { return transmittance; }

        inline InteractionType getInteractionType() const { return type; }

        RaySample sampleRay() const;
    private:
        Ray reflectedRay;
        Ray transmittedRay;

        double reflectance;
        double transmittance;

        InteractionType type;
        
        static std::default_random_engine generator;
        static std::uniform_real_distribution<double> dist;
    };
}