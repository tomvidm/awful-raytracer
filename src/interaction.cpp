#include "interaction.hpp"

#include "constants.hpp"

#include <iostream>
#include <cmath>

namespace art {
    Interaction::Interaction(const RayShapeIntersection& intersection, 
                             std::shared_ptr<Material>& exteriorMaterial, 
                             std::shared_ptr<Material>& interiorMaterial) {
        const double wavelength = intersection.sourceRay.wavelength;
        
        const double n1 = std::real(exteriorMaterial->getIndexOfRefraction(wavelength));
        const double n2 = std::real(interiorMaterial->getIndexOfRefraction(wavelength));

        const Vec3d& incident = -intersection.sourceRay.direction;
        const Vec3d& normal = intersection.normal;
        const double n12 = n1 / n2;
        const double n21 = n2 / n1;
        const double cosI = dot(incident, normal);
        const double sinTSquared = n12 * n12 * (1.0 - cosI * cosI);
        // Move to separate method
        if (sinTSquared > 1.0) {
            const double criticalAngle = asin(n21);
            const double angleOfIncidence = acos(cosI);
            //std::cout << criticalAngle << " < " << angleOfIncidence << std::endl;
            if (angleOfIncidence > criticalAngle) {
                reflectance = 1.0;
                transmittance = 0.0;
                type = InteractionType::TotalInternalReflection;
            } else {
                type = InteractionType::ReflectAndTransmit;
            }
        } else {
            type = InteractionType::ReflectAndTransmit;
        }
        
        reflectedRay = Ray(
            intersection.point,
            reflectAboutNormal(incident, normal),
            wavelength
        );

        if (type == InteractionType::ReflectAndTransmit) {
            const Vec3d t = -n12 * incident + (n12 * cosI - sqrt(1.0 - sinTSquared)) * normal;
            transmittedRay = Ray(
                intersection.point,
                t,
                wavelength
            );

            const double cosT = dot(t, -normal);

            const double sPolarizedReflectance = pow((n1 * cosI - n2 * cosT) 
                                                   / (n1 * cosI + n2 * cosT), 2.0);
            const double pPolarizedReflectance = pow((n2 * cosI - n1 * cosT) 
                                                   / (n2 * cosI + n1 * cosT), 2.0);

            reflectance = (sPolarizedReflectance + pPolarizedReflectance) / 2;
            transmittance = 1 - reflectance;
        }
    }

    RaySample Interaction::sampleRay() const {
        const double r = dist(generator);
        if (type == InteractionType::TotalInternalReflection) {
            return RaySample{
                false,
                reflectedRay
            };
        } else {
            if (r <= reflectance) {
                return RaySample{
                    false,
                    reflectedRay
                };
            } else {
                return RaySample{
                    true,
                    transmittedRay
                };
            }
        }
    }

    std::default_random_engine Interaction::generator;
    std::uniform_real_distribution<double> Interaction::dist(0.0, 1.0);
}