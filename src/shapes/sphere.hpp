#pragma once

#include "math/vector3.hpp"
#include "shape.hpp"

namespace art {
    class Sphere : public Shape {
    public:
        Sphere() : radius(1.0), origin(Vec3d()) {}
        Sphere(const double radius, const Vec3d origin) : radius(radius), origin(origin) {}

        Vec3d projectedPoint(const Vec3d& point) const;
        Vec3d getSurfaceNormal(const Vec3d& point) const;
        SurfacePoint getNearestSurfacePoint(const Vec3d& point) const;
        RayShapeIntersection getRayIntersection(const Ray& ray) const;
    private:
        const double radius;
        const Vec3d origin;
    };
}