#include <cmath>

#include "sphere.hpp"
#include "constants.hpp"

namespace art {
    Vec3d Sphere::projectedPoint(const Vec3d& point) const {
        const Vec3d direction = normalized(point - origin);
        return origin + radius * direction;
    }

    Vec3d Sphere::getSurfaceNormal(const Vec3d& point) const {
        if (magnitudeSquared(point - this->origin) < this->radius * this->radius) {
            return normalized(origin - point);
        } else {
            return normalized(point - origin);
        }
    }

    SurfacePoint Sphere::getNearestSurfacePoint(const Vec3d& point) const {
        const Vec3d direction = normalized(point - this->origin);
        const double magSquared = magnitudeSquared(direction);
        if (magSquared < radius * radius) {
            return SurfacePoint{
                origin + radius * direction,
                -direction,
                false
            };
        } else {
            return SurfacePoint{
                origin + radius * direction,
                direction,
                true
            };
        }
    }

    RayShapeIntersection Sphere::getRayIntersection(const Ray& ray) const {
        const double pp = dot(ray.origin, ray.origin);
        const double pd = dot(ray.origin, ray.direction);
        const double ps = dot(ray.origin, this->origin);
        const double sd = dot(this->origin, ray.direction);
        const double ss = dot(this->origin, this->origin);
        const double dd = dot(ray.direction, ray.direction);

        const double a = dd;
        const double b = pd - sd;
        const double c = pp - ps + ss - radius * radius;

        const double det = b * b - 4 * a * c;

        if (det < 0.0) {
            return RayShapeIntersection{
                false, Vec3d(), Vec3d(), ray, 0.0
            };
        } else if (det == 0.0) {
            const double t = -b / (2 * b) - epsilon;
            const Vec3d intersectionPoint = ray.origin + t * ray.direction;
            return RayShapeIntersection{
                true, intersectionPoint, getSurfaceNormal(intersectionPoint), ray, t
            };

        } else {
            const double t0 = (-b - sqrt(det)) / (2 * b) - epsilon;
            const double t1 = (-b + sqrt(det)) / (2 * b) - epsilon;
            if (t0 < 0.0) {
                if (t1 > 0.0) {
                    const Vec3d intersectionPoint = ray.origin + t1 * ray.direction;
                    return RayShapeIntersection{
                        true, intersectionPoint, getSurfaceNormal(intersectionPoint), ray, t1
                    };
                } else {
                    return RayShapeIntersection{
                        false, Vec3d(), Vec3d(), ray, 0.0
                    };
                }
            } else {
                const Vec3d intersectionPoint = ray.origin + t0 * ray.direction;
                return RayShapeIntersection{
                    true, intersectionPoint, getSurfaceNormal(intersectionPoint), ray, t0
                };
            }
        }
    }
}