#pragma once

#include "shape.hpp"
#include "math/vector3.hpp"

namespace art {
    class Lens : public Shape {
    public:
        Vec3d projectedPoint(const Vec3d& point) const;
        Vec3d getSurfaceNormal(const Vec3d& point) const;
        SurfacePoint getNearestSurfacePoint(const Vec3d& point) const;
        RayShapeIntersection getRayIntersection(const Ray& ray) const;
    private:
        const Vec3d origin;
        const Vec3d normal;

        const double diskThickness;
        const double diskRadius;

        // Front lens
        const double frontLensThickness;
        const double frontLensRadius;

        // Back lens
        const double backLensThickness;
        const double backLensRadius;
    };
}