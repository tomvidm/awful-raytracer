#pragma once

#include "math/vector3.hpp"
#include "shape.hpp"

namespace art {
    class Plane : public Shape {
    public:
        Plane() : origin(Vec3d()), normal(Vec3d(0.0, 0.0, 1.0)) {}
        Plane(const Vec3d& origin, const Vec3d& normal) : origin(origin), normal(normal) {}
        Plane(const Vec3d& a, const Vec3d& b, const Vec3d& c);

        Vec3d projectedPoint(const Vec3d& point) const;
        Vec3d getSurfaceNormal(const Vec3d& point) const;
        SurfacePoint getNearestSurfacePoint(const Vec3d& point) const;
        RayShapeIntersection getRayIntersection(const Ray& ray) const;

    private:
        Vec3d origin;
        Vec3d normal;
    };
}