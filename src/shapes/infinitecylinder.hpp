#pragma once

#include "shape.hpp"
#include "math/vector3.hpp"

namespace art {
    class InfiniteCylinder {
    public:
        InfiniteCylinder() = delete;
        InfiniteCylinder(const Vec3d& origin, const Vec3d& normal, const double radius)
        : origin(origin), normal(normal), radius(radius) {}
        
        Vec3d projectedPoint(const Vec3d& point) const;
        Vec3d getSurfaceNormal(const Vec3d& point) const;
        SurfacePoint getNearestSurfacePoint(const Vec3d& point) const;
        RayShapeIntersection getRayIntersection(const Ray& ray) const;
    private:
        const Vec3d origin;
        const Vec3d normal; // Normal of top disk
        const double radius;
    };
}