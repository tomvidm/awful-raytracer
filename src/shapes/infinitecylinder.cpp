#include <iostream>

#include "infinitecylinder.hpp"

namespace art {
    Vec3d InfiniteCylinder::projectedPoint(const Vec3d& point) const {
        const Vec3d planeProjection = origin + project(point - origin, normal);
        return planeProjection + radius * normalized(point - planeProjection);
    }

    Vec3d InfiniteCylinder::getSurfaceNormal(const Vec3d& point) const {
        const Vec3d planeProjection = origin + project(point - origin, normal);
        if (magnitudeSquared(point - planeProjection) > radius * radius) {
            return normalized(point - planeProjection);
        } else {
            return normalized(point - planeProjection);
        }
        
    }

    SurfacePoint InfiniteCylinder::getNearestSurfacePoint(const Vec3d& point) const {
        const Vec3d planeProjection = origin + project(point - origin, normal);
        const Vec3d surfaceNormal = normalized(point - planeProjection);
        const Vec3d surfacePoint = planeProjection + radius * surfaceNormal;

        if (magnitudeSquared(point - planeProjection) > radius * radius) {
            return SurfacePoint{
                surfacePoint,
                surfaceNormal,
                true
            };
        } else {
            return SurfacePoint{
                surfacePoint,
                -surfaceNormal,
                false
            };
        }
    }

    RayShapeIntersection InfiniteCylinder::getRayIntersection(const Ray& ray) const {
        const Vec3d diff = ray.origin - dot(ray.origin, this->normal) * this->normal;
        const Vec3d vec_a = ray.direction - dot(ray.direction, this->normal) * this->normal;
        const double a = dot(vec_a, vec_a);
        const double b = 2 * dot(vec_a, diff);
        const double c = dot(diff, diff) - this->radius * this->radius;

        const double det = b * b - 4 * a * c;

        if (det < 0.0) {
            return RayShapeIntersection{
                false
            };
        } else if (det == 0.0) {
            const double t = (-b / (2 * a));
            const Vec3d point = ray.origin + t * ray.direction;
            return RayShapeIntersection{
                true, point, getSurfaceNormal(point), ray, t
            };
        } else {
            const double sqrtDet = sqrt(det);
            const double t0 = (-b - sqrtDet) / (2 * a);
            const double t1 = (-b + sqrtDet) / (2 * a);
            if (t0 < 0.0) {
                if (t1 < 0.0) {
                    return RayShapeIntersection{
                        false
                    };
                } else {
                    const Vec3d point = ray.origin + t1 * ray.direction;
                    return RayShapeIntersection{
                        true, point, getSurfaceNormal(point), ray, t1
                    };
                }
            } else {
                const Vec3d point = ray.origin + t0 * ray.direction;
                return RayShapeIntersection{
                    true, point, getSurfaceNormal(point), ray, t0
                };
            }
        }

        
    }
}