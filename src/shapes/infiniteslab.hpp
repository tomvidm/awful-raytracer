#pragma once

#include "shape.hpp"

namespace art {
    class InfiniteSlab {
    public:
        InfiniteSlab() = delete;
        InfiniteSlab(const double thickness, const Vec3d origin, const Vec3d normal) : thickness(thickness), origin(origin), normal(normal) {}
    
        Vec3d projectedPoint(const Vec3d& point) const;
        Vec3d getSurfaceNormal(const Vec3d& point) const;
        SurfacePoint getNearestSurfacePoint(const Vec3d& point) const;
        RayShapeIntersection getRayIntersection(const Ray& ray) const;
    private:
        const double thickness;
        const Vec3d origin;
        const Vec3d normal;
    };
}