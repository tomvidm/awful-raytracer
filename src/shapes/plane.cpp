#include "plane.hpp"

namespace art {
    Plane::Plane(const Vec3d& a, const Vec3d& b, const Vec3d& c) {
        const Vec3d ab = b - a;
        const Vec3d ac = c - a;
        normal = normalized(cross(b - a, c - a));
        origin = a;
    }

    Vec3d Plane::projectedPoint(const Vec3d& point) const {
        const Vec3d diff = point - origin;
        return point - dot(diff, normal) * normal;
    }

    Vec3d Plane::getSurfaceNormal(const Vec3d& point) const {
        if (dot(point - this->origin, this->normal) > 0.0) {
            return normal;
        } else {
            return -normal;
        }
    }

    SurfacePoint Plane::getNearestSurfacePoint(const Vec3d& point) const {
        const Vec3d diff = point - this->origin;
        const double dotDiffNormal = dot(diff, this->normal);
        if (dotDiffNormal > 0.0) {
            return SurfacePoint{
                point - dotDiffNormal * this->normal,
                normal,
                true
            };
        } else {
            return SurfacePoint{
                point - dotDiffNormal * this->normal,
                -normal,
                true
            };
        }
    }

    RayShapeIntersection Plane::getRayIntersection(const Ray& ray) const {
        if (abs(dot(ray.direction, this->normal)) < 0.00005) {
            return RayShapeIntersection{
                false, Vec3d(), Vec3d(), ray
            };
        } else {
            const double numer = dot(this->normal, this->origin - ray.origin);
            const double denom = dot(this->normal, ray.direction);
            const double length = numer / denom;

            if (length > 0.0) {
                return RayShapeIntersection {
                    true, ray.origin + length * ray.direction, this->normal, ray, length
                };
            } else {
                return RayShapeIntersection {
                    false, ray.origin + length * ray.direction, this->normal, ray, length
                };
            }
        }
    }
}