#pragma once

#include "math/vector3.hpp"
#include "ray.hpp"

namespace art {
    // Structure containing information on a ray-surface intersection test
    struct RayShapeIntersection {
        const bool rayHit;
        const Vec3d point;
         Vec3d normal;
        const Ray sourceRay;
        const double distanceTravelled;
    };

    struct SurfacePoint {
        const Vec3d point;
        const Vec3d normal;
        const bool exterior;
    };

    // An interface class for a surface, independent of the geometry.
    class Shape {
    public:
        // Get the point projected onto the surface
        virtual Vec3d projectedPoint(const Vec3d& point) const = 0;

        // Get the normal at the surface for where "point" is projected
        virtual Vec3d getSurfaceNormal(const Vec3d& point) const = 0;

        // Get the nearest surface point, along with a normal and whetehr or not it is interior
        virtual SurfacePoint getNearestSurfacePoint(const Vec3d& point) const = 0;

        // Get the ray-surface intersection
        virtual RayShapeIntersection getRayIntersection(const Ray& ray) const = 0;
    };
}