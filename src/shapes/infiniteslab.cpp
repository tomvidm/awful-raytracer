#include "infiniteslab.hpp"
#include "plane.hpp"

namespace art {
    Vec3d InfiniteSlab::projectedPoint(const Vec3d& point) const {
        const Vec3d diff = point - this->origin;
        const double dotDiffNormal = dot(diff, this->normal);

        if (dotDiffNormal > 0.0) {
            return point - dotDiffNormal * this->normal;
        } else {
            if (abs(dotDiffNormal) > thickness) {
                return point - (dotDiffNormal - this->thickness) * this->normal;
            } else {
                const double d0 = dotDiffNormal;
                const double d1 = thickness - dotDiffNormal;
                if (d0 > d1) {
                    return -(point - (dotDiffNormal - this->thickness) * this->normal);
                } else {
                    return -(point - dotDiffNormal * this->normal);
                }
            }
        }
    }

    Vec3d InfiniteSlab::getSurfaceNormal(const Vec3d& point) const {
        const Vec3d diff = point - this->origin;
        const double dotDiffNormal = dot(diff, this->normal);

        if (dotDiffNormal > 0.0) {
            return normal;
        } else {
            if (abs(dotDiffNormal) > thickness) {
                return -normal;
            } else {
                const double d0 = dotDiffNormal;
                const double d1 = thickness - dotDiffNormal;
                if (d0 > d1) {
                    return normal;
                } else {
                    return -normal;
                }
            }
        }
    }

    SurfacePoint InfiniteSlab::getNearestSurfacePoint(const Vec3d& point) const {
        const Vec3d diff = point - this->origin;
        const double dotDiffNormal = dot(diff, this->normal);

        if (dotDiffNormal > 0.0) {
            return SurfacePoint{
                point - dotDiffNormal * this->normal,
                normal,
                true
            };
        } else {
            if (abs(dotDiffNormal) > thickness) {
                return SurfacePoint{
                    point - (dotDiffNormal - this->thickness) * this->normal,
                    -normal,
                    true
                };
            } else {
                const double d0 = dotDiffNormal;
                const double d1 = thickness - dotDiffNormal;
                if (d0 > d1) {
                    return SurfacePoint{
                        -(point - (dotDiffNormal - this->thickness) * this->normal),
                        normal,
                        false
                    };
                } else {
                    return SurfacePoint{
                        -(point - dotDiffNormal * this->normal),
                        -normal,
                        false
                    };
                }
            }
        }
    }

    RayShapeIntersection InfiniteSlab::getRayIntersection(const Ray& ray) const {
        const bool condA = dot(ray.origin - this->origin, this->normal) > 0.0;
        const bool condB = dot(ray.origin - (this->origin - thickness * this->normal), this->normal) > 0.0;
        const bool condC = dot(ray.direction, this->normal) > 0.0;

        if ((condA && condB && condC) || !(condA || condB || condC)) {
            return RayShapeIntersection{
                false,
                Vec3d(),
                Vec3d(),
                ray
            };
        } else {
            if (condA && !condC) {
                return Plane(this->origin, this->normal).getRayIntersection(ray);
            } else if (!condA && condB && !condC) {
                return Plane(this->origin - thickness * this->normal, this->normal).getRayIntersection(ray);
            } else if (!condA && condB && condC) {
                return Plane(this->origin, -this->normal).getRayIntersection(ray);
            } else if (!condA && !condB && condC) {
                return Plane(this->origin - thickness * this->normal, -this->normal).getRayIntersection(ray);
            }
        }
    }
}