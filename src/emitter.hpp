#pragma once

#include <random>

#include "ray.hpp"
#include "math/vector3.hpp"
#include "math/functioninterface.hpp"
#include "constants.hpp"

namespace art {
    class Emitter {
    public:
        Emitter(const Vec3d& source, const double maxTheta = PI, const double maxPhi = doublePI) 
        : sourcePoint(source), randomuniform(0.0, 1.0), randomWavelength(380.0, 740.0) {}

        Ray getRay();
    private:
        Vec3d sourcePoint;
        
        std::mt19937 generator;
        std::uniform_real_distribution<double> randomuniform;
        std::uniform_real_distribution<double> randomWavelength;
    };
}