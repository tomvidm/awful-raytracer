#include <algorithm>

#include "emitter.hpp"

namespace art {
    Ray Emitter::getRay() {
        const double z = 1.0 - 2.0 * randomuniform(generator);
        const double r = sqrt(std::max(0.0, 1.0 - z * z));
        const double phi = doublePI * randomuniform(generator);
        const double wavelength = randomWavelength(generator);
        const Vec3d direction = Vec3d(
            r * cos(phi), r * sin(phi), z
        );
    
        return Ray(
            sourcePoint,
            direction,
            wavelength
        );
    }
}