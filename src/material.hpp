#pragma once

#include <memory>

#include "math/functioninterface.hpp"

namespace art {
    class Material {
    public:
        Material(std::shared_ptr<ComplexFunction1D> ior) : iorByWavelength(ior) {}

        std::complex<double> getIndexOfRefraction(const double wavelength) const;
    private:
        std::shared_ptr<ComplexFunction1D> iorByWavelength;
        //std::shared_ptr<FunctionInterface> reflectance;
        //std::shared_ptr<FunctionInterface> transmittance;
    };
}