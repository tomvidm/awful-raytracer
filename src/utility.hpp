#pragma once

#include <cmath>

inline bool almostEqual(const double a, const double b, const double epsilon = 0.0000000001) {
    return abs(a - b) < epsilon ? true : false;
}