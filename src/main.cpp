#include <iostream>
#include <memory>
#include <random>
#include <chrono>
#include <thread>
#include <vector>

#include "shapes/plane.hpp"
#include "shapes/infiniteslab.hpp"
#include "math/linearfunction.hpp"
#include "constants.hpp"
#include "sensor.hpp"
#include "ray.hpp"
#include "material.hpp"
#include "math/vector3.hpp"
#include "emitter.hpp"
#include "interaction.hpp"

using namespace art;

std::mt19937 generator;
std::uniform_real_distribution<double> rand01(0.0, 1.0); 

Vec3d randDirection() {
    const double z = rand01(generator);
    const double r = sqrt(std::max(0.0, 1.0 - z * z));
    const double phi = doublePI * rand01(generator);

    return Vec3d(
        r * cos(phi), r * sin(phi), z
    );
}

void threadedRaycasting(
    unsigned numRays, 
    Plane& plane, 
    std::shared_ptr<Material>& mat1, 
    std::shared_ptr<Material>& mat2, 
    std::shared_ptr<LuminanceMap>& map) 
{
    Emitter emitter(Vec3d(256.0, 256.0, 128.0), PI, doublePI);
    for (unsigned i = 0; i < numRays; i++) {
        const Ray ray = emitter.getRay();
        RayShapeIntersection isec = plane.getRayIntersection(ray);
        if (isec.rayHit) {
            Interaction interaction(isec, mat1, mat2);
            RaySample sample = interaction.sampleRay();
            const float x = static_cast<float>(isec.point.x);
            const float y = static_cast<float>(isec.point.y);

            //std::cout << x << ", " << y << std::endl;
            if (sample.transmitted) {
                map->add(x, y, 0, 1.0);
            } else {
                map->add(x, y, 1, 1.0);
            }
        }
    }
}

int main() {
    Plane xy(Vec3d(), normalized(Vec3d(0.0, 0.0, 1.0)));

    const double n1 = 1.3325;
    const double n2 = 1.0;

    const unsigned numRaysPerThread = 50000000;
    const unsigned numThreads = 6;

    std::vector<std::thread> threads;

    std::shared_ptr<ComplexLinearFunction1D> ior1 = std::make_shared<ComplexLinearFunction1D>(0.0, n1);
    std::shared_ptr<ComplexLinearFunction1D> ior2 = std::make_shared<ComplexLinearFunction1D>(0.0, n2);
    std::shared_ptr<Material> mat1 = std::make_shared<Material>(ior1);
    std::shared_ptr<Material> mat2 = std::make_shared<Material>(ior2);

    std::shared_ptr<LuminanceMap> lum = std::make_shared<LuminanceMap>(512, 512);

    for (unsigned t = 0; t < numThreads; t++) {
        threads.push_back(std::thread(threadedRaycasting, numRaysPerThread, xy, mat1, mat2, lum));
    }

    for (unsigned t = 0; t < numThreads; t++) {
        threads[t].join();
    }

    lum->save("hello.bmp");

    return 0;
}